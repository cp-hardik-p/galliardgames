package com.example.galliardgames

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.galliardgames.ui.theme.GalliardGamesTheme
import kotlin.random.Random

class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalFoundationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            GalliardGamesTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val viewModel = hiltViewModel<MainViewModel>()
                    val refreshGame = viewModel.refreshGame.collectAsState()
                    if (refreshGame.value) {
                        LaunchGame(viewModel)
                        viewModel.disbaleFlag()
                    }
                    LaunchGame(viewModel)
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun LaunchGame(viewModel: MainViewModel) {
    Column {
        val data = loadNumbers()
        Text(
            text = "Galliard Game",
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 10.dp),
            textAlign = TextAlign.Center
        )

        //Greeting("Android")
        LazyVerticalGrid(
            cells = GridCells.Fixed(4),
            contentPadding = PaddingValues(8.dp)
        ) {
            items(data.size) { item ->
                Card(
                    modifier =
                    Modifier.padding(4.dp),
                    backgroundColor = Color(
                        // red = Random.nextInt(0, 255),
                        // green = Random.nextInt(0, 255),
                        // blue = Random.nextInt(0, 255)
                        Random.nextInt(0, 255)
                    )
                ) {
                    Text(
                        text = data[item].toString(),
                        fontSize = 42.sp,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.padding(24.dp)
                    )
                }
            }
        }

        Button(
            onClick = {
                viewModel.refreshGame(true)
            },
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = "Refresh",
                textAlign = TextAlign.Center
            )
        }
    }
}

@Composable
fun loadNumbers(): List<Int> {
    return (1..15).shuffled()
}
