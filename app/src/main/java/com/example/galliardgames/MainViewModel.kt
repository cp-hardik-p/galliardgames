package com.example.galliardgames

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow

class MainViewModel() : ViewModel() {

    val refreshGame = MutableStateFlow(false)


    fun refreshGame(boolean: Boolean) {
        refreshGame.value = boolean
    }

    fun disbaleFlag() {
        refreshGame.value = false
    }


}